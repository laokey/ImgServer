var express = require('express');
var router = express.Router();

var dbModel = require('../bin/dbModel.js');
var log = require('../bin/logger.js');
var fs = require('fs');

/* GET home page. */
router.get('/', function (req, res) {
    //res.render('index', { title: 'Express' });
    res.sendfile('./views/index.htm');
});

/* GET meta data */
router.post('/query', function (req, res, next) {
    if (req.body.type == 'meta') {
        var sats = req.body.satelliteid;
        var sensors = req.body.sensorid;
        var sensorarr=new Array();
        var satsarr=new Array();
        if(sats){
            satsarr=sats.split(",");
        }
        if(sensors) {
            sensorarr = sensors.split(",");
        }
        var begintime = req.body.begintime;
        var endtime = req.body.endtime;
        var btime=new Date(begintime);
        var etime=new Date(endtime);
        var cloud = parseInt(req.body.cloudpercent);
        var boundary =req.body.boundary;
        var query = dbModel.metaModel.find();
        if(sats){
            query = query.where('satelliteid').in(satsarr);
        }
        if(sensors){
            query = query.where('sensorid').in(sensorarr);
        }
        if(begintime){
            query = query.where('acquisitiontime').gte(btime);
        }
        if(endtime){
            query = query.where('acquisitiontime').lte(etime);
        }
        if(cloud){
            query = query.where('cloudpercent').lte(cloud);
        }
        if(boundary){
            query = query.where('boundary').intersects(boundary);
        }
        query = query.sort('-acquisitiontime').select('filename satelliteid sensorid ' +
            'receivestationid acquisitiontime cloudpercent sceneid orbitid scenepath scenerow ' +
            'quickviewuri boundary thumbview');
        query.exec(function(error, result){
            if (error) {
                res.json({status: 'failure'});
            } else {
                res.json(result);
            }
        });
    } else {
        next();
    }
});

/* GET province boundary */
router.post('/query', function (req, res, next) {
    if (req.body.type == 'boundary') {
        var query = {'regionid': req.body.regionid};
        //不返回字段
        var fields = {'_id' : 0};
        var options = {sort:{
            regionid: 1
        }};
        dbModel.extentModel.find(query, fields, options, function (error, result) {
            if (error) {
                res.json({status: 'failure'});
            } else {
                res.json(result);
            }
        });
    } else {
        next();
    }
});

/* GET province boundary */
router.post('/query', function (req, res, next) {
    if (req.body.type == 'belowRegion') {
        var aboveRegion = req.body.regionid;
        var query;
        if (aboveRegion.substring(2, 6) == '0000') {
            var proid = aboveRegion.substring(0, 2);
            //var con = '/^' + proid + '.*00$/';
            var con = '^' + proid + '.*00$';
            query = {$and: [{'regionid': RegExp(con)}, {'regionid': {$ne: aboveRegion}}]};
        } else if (aboveRegion.substring(4, 6) == '00') {
            var cityId = aboveRegion.substring(0, 4);
            //var con = '/^' + cityId + '/';
            var con = '^' + cityId;
            query = {$and: [{'regionid': RegExp(con)}, {'regionid': {$ne: aboveRegion}}]};
        }

        //不返回字段
        var fields = {'_id' : 0, 'boundary' : 0};
        var options = {sort:{
            regionid: 1
        }};

        dbModel.extentModel.find(query, fields, options, function (error, result) {
            if (error) {
                res.json({status: 'failure'});
            } else {
                res.json(result);
            }
        });
    } else {
        next();
    }
});

/* GET tar file */
router.get('/download', function (req, res) {
    var fileName = req.query.filename;
    if(!fileName){
        res.writeHead(404, {"Content-Type": "text/plain"});
        res.write("param is not exist");
        res.end();
        return;
    }
    dbModel.metaModel.find({filename : fileName}, {taruri : 1}, function (error, result) {
        if (error) {
            res.json({status: 'failure'});
        } else {
            if(result.length == 0){
                res.writeHead(404, {"Content-Type": "text/plain"});
                res.write("file is not exist or has been offline");
                res.end();
                return;
            }
            var taruri = result[0].taruri;

            var filePath = require('../bin/config.json').tardir + taruri;
            fs.exists(filePath, function(exists) {
                if (exists) {
                    var filestream = fs.createReadStream(filePath);
                    res.header('Content-disposition', 'attachment; filename=' + fileName);
                    res.header('Content-type', "application/octet-stream");
                    res.header('Content-Length', getFilesizeInBytes(filePath));//missing content-length
                    filestream.on('data', function(chunk) {
                        res.write(chunk);
                    });
                    filestream.on('end', function() {
                        res.end();
                    });
                } else {
                    res.writeHead(404, {"Content-Type": "text/plain"});
                    res.write("file is not exist or has been offline");
                    res.end();
                }
            });
        }
    });
});

function getFilesizeInBytes(filename) {
    var stats = fs.statSync(filename)
    var fileSizeInBytes = stats["size"]
    return fileSizeInBytes
}

module.exports = router;
