// <!--查询结果表格相关代码-->

var contextPath = "localhost";
$(function() {
	pageInit();
});
var jqtableheight = (document.body.scrollHeight - 120) * 0.6;
function pageInit() {
	jQuery("#list4")
			.jqGrid(
					{
						datatype : "local",
						height : jqtableheight,
						colNames : [ 'ID', '选择', '贴图', '拇指图', '卫星', '传感器',
								'采集时间', '文件名', '云量' ],
						colModel : [ {
							name : 'id',
							index : 'id',
							width : 30,
							//sorttype : "int"
						}, {
							name : 'chkBoxState',
							index : 'chkBoxState',
							width : 45,
							align : 'center',
							sortable : false,
							search : false,
							formatter : chkFormatter
						}, {
							name : 'chktietuState',
							index : 'chktietuState',
							width : 45,
							align : 'center',
							sortable : false,
							search : false,
							formatter : chkTietuFormatter
						}, {
							name : 'thumb',
							index : 'thumbm',
							width : 55,
							sortable : false
						}, {
							name : 'sat',
							index : 'sat',
							width : 45
						}, {
							name : 'sensor',
							index : 'sensor',
							width : 55,
							editable : true
						}, {
							name : 'date',
							index : 'date',
							width : 90,
                            sorttype : "date",
							background : 'black'
						}, {
							name : 'filename',
							index : 'filename',
							width : 30,
							sorttype : "int"
						}, {
							name : 'cloud',
							index : 'cloud',
							width : 45,
                            sorttype : "int"
						},

						],
                        rowNum:5000,
						multiselect : false,
						width : ($(window).width),
						pager : '#gridPager',
						sortname : 'id',
						mtype : "post",
						viewrecords : true, // 是否显示行数
						sortorder : "desc",
						caption : "查询结果列表",
						pginput : false,
						pgbuttons : false,
                        hidegrid:false,
						ondblClickRow : function(id) {

						},
						// 此处对所有选中表格单元格添加事件
						onCellSelect : function(rowId, index, contents, event) {
							// 此处可以设定选择的表格框时间和属性
							var rowData = $("#list4").jqGrid('getRowData',
									rowId);
							if (contents.indexOf("selectcheckbox") > 0) {
								var test = rowData.chkBoxState;
								if (test.indexOf("checked") > 0) {
									$("#list4").jqGrid('setRowData', rowId, {
										chkBoxState : false
									});
								} else {
									$("#list4").jqGrid('setRowData', rowId, {
										chkBoxState : true
									});

								}
							}
							if (contents.indexOf("tietucheckbox") > 0) {
								var test2 = rowData.chktietuState;
								if (test2.indexOf("checked") > 0) {
									$("#list4").jqGrid('setRowData', rowId, {
										chktietuState : false
									});
									map.removeLayer(TietuLayerArray[rowId - 1]);
								} else {
									$("#list4").jqGrid('setRowData', rowId, {
										chktietuState : true
									});
									map
											.getView()
											.setCenter(
													[
															Allpostjson[rowId - 1]["boundary"]["coordinates"][0][1][0],
															Allpostjson[rowId - 1]["boundary"]["coordinates"][0][2][1] ]);
									map.getView().setZoom(9);
									map.removeLayer(TietuLayerArray[rowId - 1]);
									map.addLayer(TietuLayerArray[rowId - 1]);
								}
							}
							var valuecell = $("#list4").getCell(rowId,
									'filename');
							// 当点击拇指图时出现详情页面
							if (index == 3) {
								showBig(valuecell);
							}
						},
						loadComplete : function() {
							$("#list4").setGridWidth(
									document.body.clientWidth * 0.249);

						}
					});
	$("#list4").navGrid('#gridPager', {
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : false
	});
	$("#list4").navGrid("#gridPager").navButtonAdd("#gridPager", {
		caption : "落图全选",
		onClickButton : function() {
			selectallluotu();
		}
	});
	$("#list4").navGrid("#gridPager").navButtonAdd("#gridPager", {
		caption : "贴图全选",
		onClickButton : function() {
			selectalltietu();
		}
	});

	// 可以隐藏某一列
	jQuery("#list4").setGridParam().hideCol("id").trigger("reloadGrid");
	jQuery("#list4").setGridParam().hideCol("filename").trigger("reloadGrid");
	// 自定义拇指图列格式
	function thumbFormatter(cellvalue, options, rowdata) {
		var aa = "<img class='thumbimg' src='kuaishiimg/meta.jpg' onclick=\"showBig("
				+ options.rowId + ");\">";
		return aa;
	}
	// 定义贴图图标样式
	function tietuFormatter(cellvalue, options, rowdata) {
		var aa = "<input type='checkbox' id='tietucheckbox"
				+ options.rowId
				+ "' name='tietucheckbox' value='tietucheckbox'  style='width:20px;height:20px;' onclick=\"TietuCheck(this,"
				+ options.rowId + ");\">";
		return aa;
	}
	// 定义贴图图标样式
	function selectFormatter(cellvalue, options, rowdata) {
		var aa = "<input type='checkbox' id='selectcheckbox"
				+ options.rowId
				+ "' name='selectcheckbox' value='selectcheckbox'  style='width:20px;height:20px;' onclick=\"selectCheck(this,"
				+ options.rowId + ");\">";
		return aa;
	}
	function chkFormatter(cellValue, options, rowObjects) {
		if (!cellValue) {
			cellValue = "<input id='selectcheckbox"
					+ options.rowId
					+ "' type='checkbox' class='cbox' role='checkbox' style='width:20px;height:20px;'onclick=\"selectCheck(this,"
					+ options.rowId + ");\">";
		} else {
			cellValue = "<input id='selectcheckbox"
					+ options.rowId
					+ "' type='checkbox' class='cbox' role='checkbox' checked='' style='width:20px;height:20px;'onclick=\"selectCheck(this,"
					+ options.rowId + ");\">";
		}
		return cellValue;
	}
	function chkTietuFormatter(cellValue, options, rowObjects) {
		if (!cellValue) {
			cellValue = "<input id='tietucheckbox"
					+ options.rowId
					+ "' type='checkbox' class='cbox' role='checkbox' style='width:20px;height:20px;'onclick=\"TietuCheck(this,"
					+ options.rowId + ");\">";
		} else {
			cellValue = "<input id='tietucheckbox"
					+ options.rowId
					+ "' type='checkbox' class='cbox' role='checkbox' checked='' style='width:20px;height:20px;'onclick=\"TietuCheck(this,"
					+ options.rowId + ");\">";
		}
		return cellValue;
	}

}
function selectallluotu() {
	var objlen = $("#list4").jqGrid("getRowData").length;
	if (document.getElementById("selectallluotu").style.color == 'red') {
		for (var i = 0; i < objlen; i++) {
			var idname = 'selectcheckbox' + (i + 1);
			document.getElementById(idname).checked = true;
			document.getElementById(idname).onclick();
		}
		document.getElementById("selectallluotu").style.color = 'blue';
	} else {
		for (var i = 0; i < objlen; i++) {
			var idname = 'selectcheckbox' + (i + 1);
			document.getElementById(idname).checked = false;
			document.getElementById(idname).onclick();
		}
		document.getElementById("selectallluotu").style.color = 'red';
		if (shpluotu) {
			shpluotu = null;
		}
	}
}
function selectalltietu() {
    var loadinghtml = "<img  src='images/loading.gif' style='margin-left:50px;margin-top:20px;'/>"
    //jbox全局设置：
    var jBoxConfig={};
    jBoxConfig.defaults={
        top:'45%',
    }
    $.jBox.setDefaults(jBoxConfig);
    $.jBox(loadinghtml, {
        title: "贴图中...",
        width: 300,
        height: 120,
        buttons: { "停止贴图":"1"},
        submit: function (v, o, f) {
            if (v == 1) {
                return true; // close the window
                document.execCommand("stop");
            }
        }
    });
    var objlen = $("#list4").jqGrid("getRowData").length;
    if (document.getElementById("selectalltietu").style.color == 'red') {
        for (var i = 0; i < objlen; i++) {
            var idname = 'tietucheckbox' + (i + 1);
            if (document.getElementById(idname).checked == false) {
                document.getElementById(idname).checked = true;
                document.getElementById(idname).onclick();
            }
        }
        document.getElementById("selectalltietu").style.color = 'blue';
    } else {
        for (var i = 0; i < objlen; i++) {
            var idname = 'tietucheckbox' + (i + 1);
            if (document.getElementById(idname).checked == true) {
                document.getElementById(idname).checked = false;
                document.getElementById(idname).onclick();
            }
        }
        document.getElementById("selectalltietu").style.color = 'red';
    }
    $.jBox.close();
}