//添加注释
//我GitLab上修改的
//dev分支上做的修改
function queryarea(name,value) {
    //改变鼠标手势
    mapActive("pan");
    if(shpluotu)
    {
        shpluotu=null;
    }
    //省市县界矢量图层
    vectorLayer = new ol.layer.Vector({
        source: vectorSources,
        style: styleFunction
    });
    if(name == 'province'){
        document.getElementById("s2").options.length = 1;
        document.getElementById("s3").options.length = 1;
    }else if(name == 'city'){
        document.getElementById("s3").options.length = 1;
    }
    var town = new Array();
    $.ajax({
        url: 'query',
        type: 'POST',
        dataType: 'json',
        data: {
            type: 'boundary',
            regionid: value
        },
        //请求成功后触发
        error: function (d) {
            alert("出错了！！:" + d);
        },
        success: function (d) {
            if (vectorSources) {
                vectorSources.clear();
            }
            DrawXZline(d);
        }
    });
    $.ajax({
        url: 'query',
        type: 'POST',
        dataType: 'json',
        data: {
            type: 'belowRegion',
            regionid: value
        },
        error: function (d) {
            alert("出错了！！:" + d);
        },
        success: function (d) {
            if(name == 'province'){
                for (var o in d) {
                    document.getElementById("s2").options
                        .add(new Option(d[o].name, d[o].regionid));
                }
            }else if(name == 'city'){
                for (var o in d) {
                    if (!contains(town, JSON.stringify(d[o].name))) {
                    document.getElementById("s3").options
                        .add(new Option(d[o].name, d[o].regionid));
                        town.push(JSON.stringify(d[o].name));
                }
                }
            }
        }
    });
}
//字符串包含判断
function contains(arr, obj) {
    var i = arr.length;
    while (i--) {
        if (arr[i] === obj) {
            return true;
        }
    }
    return false;
}