//local changes
function check() {
    //清除上一次的查询结果
    if (vectorSourceOutline) {
        vectorSourceOutline.clear();
    }
    if (queryresultjson) {
        queryresultjson = null;
    }
    if(querygeojson){
        querygeojson=null;
    }
	if(tempSource){
        tempSource.clear();
    }
    //清除贴图
    if (TietuLayerArray.length) {
        for (var i = 0; i < TietuLayerArray.length; i++)
            map.removeLayer(TietuLayerArray[i]);
    }
    //落图清除
    if (shpluotu) {
        shpluotu = null;
    }
    //卫星一选中的数组
    var idArray1 = getAllSelectedValues('sateliteid');
    var satelArrary=new Array();
    document.getElementById("sensorid").value = idArray1;
    if(document.getElementById("z3mux").checked||document.getElementById("z3nad").checked
        ||document.getElementById("z3tlc").checked){
        satelArrary.push("ZY3-1");
    }
    if(document.getElementById("gf1pms").checked){
        satelArrary.push("GF1");
    }
    if(document.getElementById("gf2pms").checked){
        satelArrary.push("GF2");
    }
    document.getElementById("satelite").value = satelArrary.toString();
    //读取当前图层的要素转化为GEOJSON
    if (vectorSources.getFeatures().length > 0) {
        var format = new ol.format.GeoJSON();
        /*so far only support single feature*/
        querygeojson = JSON.parse(format.writeGeometry(vectorSources.getFeatures()[0].getGeometry()));
    }
    if(checkParams()){
        return true;
    }else{
        var jBoxConfig={};
        jBoxConfig.defaults={
            top:'45%',
        }
        $.jBox.setDefaults(jBoxConfig);
        $.jBox.tip("请至少选择一个查询条件进行查询.....");
        return false;
    }
}

function checkParams(){
    var sensor=document.getElementById("sensorid").value;
    var begintime=document.getElementById("begintime").value;
    var endtime=document.getElementById("endtime").value;
    var CloudPercent=document.getElementById("CloudPercent").value;
    var featureCnt=vectorSources.getFeatures().length;
    if(sensor||begintime||endtime||CloudPercent||featureCnt!=0){
        return true;
    }
    return false;
}

function getAllSelectedValues(subname) {
    var array = new Array();
    var boxes = document.getElementsByName(subname);
    if (!boxes) {
        return array;
    }
    for (var i = 0; i < boxes.length; i++) {
        if (boxes[i].checked) {
            array[array.length] = boxes[i].value;
        }
    }
    return array;
}
//查询返回数据
function queryresult() {
    var loadinghtml = "<img  src='images/loading.gif' style='margin-left:50px;margin-top:20px;'/>"
    if (check()) {
        var data={
                type: 'meta',
                satelliteid:$("#satelite").val(),
                sensorid: $("#sensorid").val(),
                begintime: $("#begintime").val(),
                endtime: $("#endtime").val(),
                cloudpercent: $("#CloudPercent").val(),
                boundary: querygeojson
        };
        //jbox全局设置：
        var jBoxConfig={};
        jBoxConfig.defaults={
            top:'45%',
        }
        $.jBox.setDefaults(jBoxConfig);
        $.jBox(loadinghtml, {
            title: "查询中",
            width: 300,
            height: 80,
            buttons: { }
        });
        $.ajax({
            url: "query",
            type: "post",
            dataType: 'json',
            contentType:"application/json",
            data: JSON.stringify(data),
            error: function (d) {
                //alert("出错了！！:" + d);
                $.jBox.tip("查询出现错误，请刷新后重试.....");
                $.jBox.close();
            },
            success: function (d) {
                var status= d.status;
                if(status){
                    $.jBox.tip("查询出现错误，请刷新后重试.....");
                    $.jBox.close();
                }
                if (d.length == 0) {
                    $.jBox.close();
                    $.jBox.tip("暂无数据");
                    jQuery("#list4").jqGrid("clearGridData");
                }
                else {
                    //清除前一次查询的列表视图
                    jQuery("#list4").jqGrid("clearGridData");
                    //清除前一次查询的shape落图以及贴图
                    if (shpluotu) {
                        shpluotu = null;
                    }
                    queryresultjson = d;
                    //本次查询的矢量落图数据
                    shpluotu = PutJson2Feature(d);
                    //显示本次查询结果
                    ShowSearch(d);
                    document.getElementById("satelite").value="";
                    document.getElementById("resultdiv").click();
                    $.jBox.close();
                }
            }
        });

    }

}

