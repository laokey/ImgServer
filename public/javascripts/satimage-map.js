/*-----------------------maptooles工具条-----------------*/
//全局绘制要素变量
var draw;
//地图工具控件变量
var mapType = "pan";
function mapActive(type) {
    switch (type) {
        case "pan":
            changeMapImg(mapType, type,
                'images/mapicon/grey_icon_hand_h.gif');
            map.removeInteraction(draw);
            mapType = type;
            break;
        case "selRect":
            //画矩形框选择
            changeMapImg(mapType, type, 'images/mapicon/grey_icon_select_h.gif');
            if (draw) {
                map.removeInteraction(draw);
            }
            if (shpluotu) {
                shpluotu = null;
            }
            if (vectorSources) {
                vectorSources.clear();
            }
            addInteraction("Box");
            mapType = type;
            break;
        case "selPol":
            //画多边形选取
            changeMapImg(mapType, type,
                'images/mapicon/polygon_icon_select_h.gif');
            if (vectorSources) {
                vectorSources.clear();
            }
            if (shpluotu) {
                shpluotu = null;
            }
            map.removeInteraction(draw);
            addInteraction("Polygon");
            mapType = type;
            break;
        case "fullMap":
            map.getView().setZoom(4);
            var center = ol.proj.transform([107.07, 38.02], 'EPSG:4326',
                'EPSG:4326');
            map.getView().setCenter(center);
            break;
        case "clear":
            clearview();
            break;
    }
}
//点击改变工具图标
function changeMapImg(type1, type2, imgUrl) {
    document.getElementById(type2).src = imgUrl;
    switch (type1) {
        case 'pan':
            document.getElementById(type1).src = 'images/mapicon/grey_icon_hand.gif';
            break;
        case 'selRect':
            document.getElementById(type1).src = 'images/mapicon/grey_icon_select.gif';
            break;
        case 'selPol':
            document.getElementById(type1).src = 'images/mapicon/polygon_icon_select.gif';
            break;
    }
}
//清除视图
function clearview() {
    document.getElementById("shpinput").value = "";
    if (vectorSourceOutline != null) {
        vectorSourceOutline.clear();//清空图层元素
    }
    if (vectorSources) {
        vectorSources.clear();
    }
    if (shpluotu) {
        shpluotu = null;
    }
	if(tempSource){
        tempSource.clear();
    }
    document.getElementById('file').value = '';
    mapActive("pan");
    //清除贴图
    if (TietuLayerArray.length) {
        for (var i = 0; i < TietuLayerArray.length; i++)
            map.removeLayer(TietuLayerArray[i]);
    }
    jQuery("#list4").jqGrid("clearGridData");
    //清除前一次查询的shape落图以及贴图
    if (shpluotu) {
        shpluotu = null;
    }
    document.getElementById("searchdiv").click();
}
//鼠标滑动显示经纬度
var mousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(4),
    projection: 'EPSG:4326',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position'),
    undefinedHTML: '&nbsp;'
});
//落图下载
function DownloadShp() {
    mapActive("pan");
    if(!queryresultjson||queryresultjson.length==0){
        var jBoxConfig={};
        jBoxConfig.defaults={
            top:'45%',
        }
        $.jBox.setDefaults(jBoxConfig);
        $.jBox.tip("请先进行数据查询.....");
    }else {
        var selectshpjson = {};
        selectshpjson['type'] = 'FeatureCollection';
        selectshpjson['features'] = [];
        var luotuAttSatelite = new Array();
        var luotuAttfilename = new Array();
        var luotuAttSensor = new Array();
        var luotuAttCloud = new Array();
        var luotuAttDate = new Array();
        var luotuAttStation = new Array();
        var luotuAttSceneid = new Array();
        var luotuAttOrbitid = new Array();
        var luotuAttScenepath = new Array();
        var luotuAttScenerow = new Array();
        var selectcnt = 0;

        for (i = 0; i < queryresultjson.length; i++) {
            var idname = 'selectcheckbox' + (i + 1);
            var coord = Allpostjson[i]["boundary"]["coordinates"];
            LuotushpArray[i] = coord;
            luotuAttfilename[i] = queryresultjson[i]["filename"],
                luotuAttSatelite[i] = queryresultjson[i]["satelliteid"];
            luotuAttSensor[i] = queryresultjson[i]["sensorid"];
            luotuAttCloud[i] = queryresultjson[i]["cloudpercent"];
            luotuAttDate[i] = queryresultjson[i]["acquisitiontime"];
            luotuAttStation[i] = queryresultjson[i]["receivestationid"];
            luotuAttSceneid[i] = queryresultjson[i]["sceneid"];
            luotuAttOrbitid[i] = queryresultjson[i]["orbitid"];
            luotuAttScenepath[i] = queryresultjson[i]["scenepath"];
            luotuAttScenerow[i] = queryresultjson[i]["scenerow"];
            if (document.getElementById(idname).checked == true) {
                selectcnt++;
            }
            var newFeature = {
                "type": "Feature",
                "geometry": {
                    "type": "MultiPolygon",
                    "coordinates": [LuotushpArray[i]]
                },
                "properties": {
                    "filename": luotuAttfilename[i],
                    "sateliteid": luotuAttSatelite[i],
                    "sensorid": luotuAttSensor[i],
                    "cloudpercent": luotuAttCloud[i],
                    "date": FormatDate(luotuAttDate[i]),
                    "receivestation": luotuAttStation[i],
                    "sceneid": luotuAttSceneid[i],
                    "orbitid": luotuAttOrbitid[i],
                    "scenepath": luotuAttScenepath[i],
                    "scenerow": luotuAttScenerow[i]
                }
            }
            selectshpjson['features'].push(newFeature);
        }
        if (selectcnt != 0) {
            var json = JSON.stringify(selectshpjson);
            post("http://" + hostport + ":3000/convertJson", {
                json: json,
                outputName: "shape.zip"
            });
        }else{
            var jBoxConfig={};
            jBoxConfig.defaults={
                top:'45%',
            }
            $.jBox.setDefaults(jBoxConfig);
            $.jBox.tip("请至少选择一个落图下载.....");
        }
    }
}
//落图下载post请求
function post(URL, PARAMS) {
    var temp = document.createElement("form");
    temp.action = URL;
    temp.method = "post";
    temp.style.display = "none";
    for (var x in PARAMS) {
        var opt = document.createElement("textarea");
        opt.name = x;
        opt.value = PARAMS[x];
        temp.appendChild(opt);
    }
    document.body.appendChild(temp);
    temp.submit();
    return temp;
}
// 经纬度定位检查
function DrawLonLatCheck() {
    var upperLeftLat = document.getElementById("upperLeftLat").value;
    if (upperLeftLat != "") {
        var num=Number(upperLeftLat);
        if(isNaN(Number(upperLeftLat))){
           transangle();
        }
    }
    var upperLeftLat = document.getElementById("upperLeftLat").value;
    var upperLeftLong = document.getElementById("upperLeftLong").value;
    var lowerRightLat = document.getElementById("lowerRightLat").value;
    var lowerRightLong = document.getElementById("lowerRightLong").value;
    if (upperLeftLong != "") {
        if ( upperLeftLong > 180 || upperLeftLong < 0) {
            alert("请输入正确的左上角经度!");
            return false;
        }
    }
    if (lowerRightLat != "") {
        if (lowerRightLat > 90 || lowerRightLat < 0) {
            alert("请输入正确的右下角纬度格式!");
            return false;
        }
    }
    if (lowerRightLong != "") {
        if (lowerRightLong > 180 || lowerRightLong < 0) {
            alert("请输入正确的右下角经度格式!");
            return false;
        }
    }
    return true;
}
//绘制经纬度框
function DrawLonLat(){
    if(DrawLonLatCheck()){
        //鼠标手势
        mapActive("pan");
        // 绘制之前清除掉其它图层要素
        if (shpluotu)
            shpluotu = null;
        if (vectorSources) {
            vectorSources.clear();
        }
        var upperLeftLat = document.getElementById("upperLeftLat").value;
        var upperLeftLong = document.getElementById("upperLeftLong").value;
        var lowerRightLat = document.getElementById("lowerRightLat").value;
        var lowerRightLong = document.getElementById("lowerRightLong").value;
        if (upperLeftLat.indexOf("\'") > 0) {
            upperLeftLat=dfm2Decimal(upperLeftLat);
            upperLeftLong=dfm2Decimal(upperLeftLong);
            lowerRightLat=dfm2Decimal(lowerRightLat);
            lowerRightLong=dfm2Decimal(lowerRightLong);
        }
        var c1 = ol.proj.transform([upperLeftLong * 1.0, lowerRightLat * 1.0],
            'EPSG:4326', 'EPSG:4326');
        var c2 = ol.proj.transform([upperLeftLong * 1.0, upperLeftLat * 1.0],
            'EPSG:4326', 'EPSG:4326');
        var c3 = ol.proj.transform([lowerRightLong * 1.0, upperLeftLat * 1.0],
            'EPSG:4326', 'EPSG:4326');
        var c4 = ol.proj.transform([lowerRightLong * 1.0, lowerRightLat * 1.0],
            'EPSG:4326', 'EPSG:4326');
        var LonLatgeojsonObject = {
            'type': 'FeatureCollection',
            'crs': {
                'type': 'name',
                'properties': {
                    'name': 'EPSG:4326'
                }
            },
            'features': [{
                'type': 'Feature',
                'geometry': {
                    'type': 'MultiPolygon',
                    'coordinates': [[[c1, c2, c3, c4,c1]]]
                }
            },

            ]
        };

        vectorSources = new ol.source.Vector({
            features: (new ol.format.GeoJSON()).readFeatures(LonLatgeojsonObject)
        });
        vectorLayer = new ol.layer.Vector({
            source: vectorSources,
            style: styleFunction
        });
        map.addLayer(vectorLayer);
        var tempextent = vectorSources.getFeatures()[vectorSources
            .getFeatures().length - 1].getGeometry().getExtent();
        map.getView().fit(tempextent, /** @type {ol.Size} */
            (map.getSize()));
    }
}
//清除定位
function clearinput() {
    document.getElementById("upperLeftLong").value = "";
    document.getElementById("upperLeftLat").value = "";
    document.getElementById("lowerRightLong").value = "";
    document.getElementById("lowerRightLat").value = "";
    if (vectorSources != null) {
        vectorSources.clear();
    }
}
//角度转换
function transangle() {
    var upperLeftLong = document.getElementById("upperLeftLong").value;
    var upperLeftLat = document.getElementById("upperLeftLat").value;
    var lowerRightLong = document.getElementById("lowerRightLong").value;
    var lowerRightLat = document.getElementById("lowerRightLat").value;
    // 十进制转度分秒
    if (upperLeftLong.indexOf("\'") < 0) {
        document.getElementById("upperLeftLong").value = dfm2Decimal(upperLeftLong);
    }
    // 度分秒转十进制
    else {
        document.getElementById("upperLeftLong").value = decimal2Dfm(upperLeftLong)*1.0;
    }
    if (upperLeftLat.indexOf("\'") < 0) {
        document.getElementById("upperLeftLat").value = dfm2Decimal(upperLeftLat);
    }
    // 度分秒转十进制
    else {
        document.getElementById("upperLeftLat").value = decimal2Dfm(upperLeftLat)*1.0;
    }
    if (lowerRightLong.indexOf("\'") < 0) {
        document.getElementById("lowerRightLong").value = dfm2Decimal(lowerRightLong);
    }
    // 度分秒转十进制
    else {
        document.getElementById("lowerRightLong").value = decimal2Dfm(lowerRightLong)*1.0;
    }
    if (lowerRightLat.indexOf("\'") < 0) {
        document.getElementById("lowerRightLat").value = dfm2Decimal(lowerRightLat);
    }
    // 度分秒转十进制
    else {
        document.getElementById("lowerRightLat").value = decimal2Dfm(lowerRightLat)*1.0;
    }

}
//度分秒转十进制
function dfm2Decimal(value){
    var DFM;
    if (value > 0) {
        var du = Math.floor(value);
        var fen = Math.floor((value - du) * 60);
        var miao = ((value - du) * 60 - fen).toFixed(2)*60;
        DFM = du + "°" + fen + "\'" + miao + "\"";
    } else {
        var du = Math.ceil(value);
        var fen = Math.floor(-(value - du) * 60);
        var miao = (-(value - du) * 60 - fen).toFixed(2)*60;
        DFM = du + "°" + fen + "\'" + miao + "\"";
    }
    return DFM;

}
//十进制转度分秒
function decimal2Dfm(value){
    var AngleArray = value.split(/[°'"]/);
    var Angle;
    if (AngleArray[0] > 0) {
        Angle = ((AngleArray[2] * 1.0 / 60 + AngleArray[1] * 1.0) / 60 + AngleArray[0] * 1.0)
            .toFixed(3);
    } else {
        Angle = (-(AngleArray[2] * 1.0 / 60 + AngleArray[1] * 1.0) / 60 + AngleArray[0] * 1.0)
            .toFixed(3);
    }
    return (Angle * 1.0).toFixed(2);
}

//添加绘制要素
function addInteraction(value) {
    vectorSources = new ol.source.Vector({
        /*features: (new ol.format.GeoJSON()).readFeatures(geojsonObj)*/
    });

    map.removeLayer(vectorLayer);
    vectorLayer = new ol.layer.Vector({
        source: vectorSources,
        style: styleFunction
    });
    map.addLayer(vectorLayer);
    if (value !== 'None') {
        var geometryFunction, maxPoints;
        if (value === 'Square') {
            value = 'Circle';
            geometryFunction = ol.interaction.Draw.createRegularPolygon(4);
        } else if (value === 'Box') {
            value = 'LineString';
            maxPoints = 2;
            geometryFunction = function (coordinates, geometry) {
                if (!geometry) {
                    geometry = new ol.geom.Polygon(null);
                }
                var start = coordinates[0];
                var end = coordinates[1];
                geometry.setCoordinates([[start, [start[0], end[1]], end,
                    [end[0], start[1]], start]]);
                // 此处将地图格式的坐标转化为经纬度形式
                sourceProj =projection;
                return geometry;
            };
        }

        draw = new ol.interaction.Draw({
            source: vectorSources,
            type: /** @type {ol.geom.GeometryType} */
                (value),
            geometryFunction: geometryFunction,
            maxPoints: maxPoints
        });
        // 清除上一次画图
        var lastFeature;
        var removeLastFeature = function () {
            if (lastFeature)
                vectorSources.removeFeature(lastFeature);
        };
        draw.on('drawstart', function (e) {
            // Filter on all BUT points
            if (value != 'Point') {
                vectorSources.clear(); // implicit remove of last feature.
            }
        });

        // When a drawing ends for a point feature, remove the last feature
        draw.on('drawend', function (e) {
            // Filter on points ONLY
            if (value == 'Point') {
                removeLastFeature();
            }
            lastFeature = e.feature;
        });

        map.addInteraction(draw);
    }
}
/*------------------地图相关核心-----------------------*/
//规定坐标系
var projection = ol.proj.get('EPSG:4326');
var tileSize = 256;
// 添加天地图WMS图层
var urlvec = 'http://t0.tianditu.com/DataServer?T=vec_c&x={x}&y={y}&l={z}';
var urlcva = 'http://t0.tianditu.com/DataServer?T=cva_c&x={x}&y={y}&l={z}';
var TDT1 = new ol.layer.Tile({
    source: new ol.source.XYZ({
        maxZoom: 16,
        projection: projection,
        tileSize: tileSize,
        tileUrlFunction: function (tileCoord) {
            return urlvec.replace('{z}', (tileCoord[0]).toString()).replace(
                '{x}', tileCoord[1].toString()).replace('{y}',
                (-tileCoord[2] - 1).toString());
        },

    })
});
var TDT2 = new ol.layer.Tile({
    source: new ol.source.XYZ({
        maxZoom: 16,
        projection: projection,
        tileSize: tileSize,
        tileUrlFunction: function (tileCoord) {
            return urlcva.replace('{z}', (tileCoord[0]).toString()).replace(
                '{x}', tileCoord[1].toString()).replace('{y}',
                (-tileCoord[2] - 1).toString());
        },

    })
});
//添加本地矢量图层
var locallayer = new ol.layer.Tile({
    source: new ol.source.TileWMS({
        url: "http://"+hostport+":7090/geoserver/china/wms",
        params: {
            LAYERS: 'china:province',
            VERSION: '1.1.0'
        }
    })
});
locallayer.setVisible(false);
//定义地图矢量图层
vectorSource = new ol.source.Vector({
    wrapX: false
});
vectorLayer = new ol.layer.Vector({
    source: vectorSource,
    style: new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0)'
        }),
        stroke: new ol.style.Stroke({
            color: 'blue',
            width: 2
        }),
        image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
                color: '#ffcc33'
            })
        })
    })
});
var map = new ol.Map({
    controls: ol.control.defaults({
 /*       attributionOptions: /!** @type {olx.control.AttributionOptions} *!/
            ({
                collapsible: false
            })*/
    }).extend([mousePositionControl]), //鼠标移动显示经纬度
    //选中样式select
    //interactions : ol.interaction.defaults().extend([ select ]),
    target: 'map',
    layers: [TDT1, TDT2, locallayer, vectorLayer],
    view: new ol.View({
        center: [120, 30],
        projection: projection,
        zoom: 5,
        minZoom: 2
    })
});
map.addEventListener("click")
//初始设置本地矢量图为隐藏
//选中map类型变化
function maptypechange() {
    var obj = document.getElementById("maptype");
    var type = obj.options[obj.selectedIndex].text;
    if (type == '天地图') {
        TDT1.setVisible(true);
        TDT2.setVisible(true);
        locallayer.setVisible(false);
    } else {
        // map.addLayer(locallayer);
        TDT1.setVisible(false);
        TDT2.setVisible(false);
        locallayer.setVisible(true);
    }
}