//存放结果列表数据相关画图及数据构建函数
// Allpostjson将快视贴图数据灌入数组 */
function PutJson2Feature(Allpostjson) {
    LuotushpArray = [];
    var TietuUrl = new Array(Allpostjson.length);
    var TietuExtent = new Array(Allpostjson.length);
    var ToatalLuoTuCoord = "";
    var geojson = {};
    geojson['type'] = 'FeatureCollection';
    geojson['features'] = [];
    for (var i = 0; i < Allpostjson.length; i++) {
        var coord = Allpostjson[i]["boundary"]["coordinates"];
        LuotushpArray[i] = coord;
        ToatalLuoTuCoord += "[[[" + coord + "]]],";
        var tietuuri = Allpostjson[i]["quickviewuri"];
        tietuuri = tietuuri.replace("jpg", "png");
        TietuUrl[i] = tietuuri;
        TietuExtent[i] = [Allpostjson[i]["boundary"]["coordinates"][0][1][0],
            Allpostjson[i]["boundary"]["coordinates"][0][2][1],
            Allpostjson[i]["boundary"]["coordinates"][0][3][0],
            Allpostjson[i]["boundary"]["coordinates"][0][0][1]];
        TietuLayerArray[i] = new ol.layer.Image({
            source: new ol.source.ImageStatic({
                url: TietuUrl[i],
                projection: projection,
                imageExtent: TietuExtent[i],
            })
        });
        var newFeature = {
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [coord]
            },
            "properties": {
                "title": "aa",
                "description": "bb"
            }
        }
        geojson['features'].push(newFeature);
    }
    return geojson;

}
function TietuCheck(para, rowId) {
    if (para.checked) {
        map.getView()
            .setCenter(
            [
                Allpostjson[rowId - 1]["boundary"]["coordinates"][0][1][0],
                Allpostjson[rowId - 1]["boundary"]["coordinates"][0][2][1]]);
        map.getView().setZoom(9);
        map.removeLayer(TietuLayerArray[rowId - 1]);
        map.addLayer(TietuLayerArray[rowId - 1]);
    } else {
        map.removeLayer(TietuLayerArray[rowId - 1]);
    }
}

function selectCheck(para, rowId) {
    if(para.checked) {
        map.getView().setCenter(
            [
                Allpostjson[rowId - 1]["boundary"]["coordinates"][0][1][0],
                Allpostjson[rowId - 1]["boundary"]["coordinates"][0][2][1]]);
        map.getView().setZoom(9);
        var obj = Allpostjson[rowId-1];
        var boundary = obj['boundary'];
        // Read the GeoJSON geometries using the format class
        var format = new ol.format.GeoJSON();
        var geometry = format.readGeometry(boundary);
        // Create a feature using the geometry
        var feature = new ol.Feature({
            geometry: geometry
        });
        // Add feature to the source of some vector layer
        tempSource.addFeature(feature);
        vectortempLayer = new ol.layer.Vector({
            source: tempSource,
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(200,230,200, 0)'
                }),
                stroke: new ol.style.Stroke({
                    color: 'green',
                    width: 2
                })
            })
        });
        map.addLayer(vectortempLayer);
    }else{removetemply();}
}

// 点击某单元格的onclick事件
function showBig(rowId) {
    //alert(JSON.stringify(queryresultjson));
    for (var i = 0; i < queryresultjson.length; i++) {
        //alert(JSON.stringify(queryresultjson[i]));
        if (queryresultjson[i].filename == rowId) {
            var satelliteid = queryresultjson[i].satelliteid;
            var sensorid = queryresultjson[i].sensorid;
            var acquisitiontime = queryresultjson[i].acquisitiontime;
            var acquisitiontime = FormatDate(acquisitiontime);
            var cloudpercent = queryresultjson[i].cloudpercent + "%";
            var quickviewuri = queryresultjson[i].quickviewuri;
            var boundary = queryresultjson[i].boundary;
            var filename = queryresultjson[i].filename;
            var orbitid = queryresultjson[i].orbitid;
            var receivestationid = queryresultjson[i].receivestationid;
            var scenepath = queryresultjson[i].scenepath;
            var scenerow = queryresultjson[i].scenerow;
            var scenepr = JSON.stringify(scenepath) + "/"
                + JSON.stringify(scenerow);
            var resolution;
            if (sensorid == "MUX") resolution = "5.8m";
            else if (sensorid == "NAD") resolution = "2.1m";
            else if (sensorid == "FWD" || sensorid == "BWD") resolution = "3.6m";
            else if (sensorid == "PMS1" || sensorid == "PMS2") resolution = "8m";
            //var taruri = queryresultjson[i].taruri;
            var lu = boundary["coordinates"][0][0];
            lu = JSON.stringify(lu).replace(",", "/").replace("[", "")
                .replace("]", "");
            var ld = boundary["coordinates"][0][1];
            ld = JSON.stringify(ld).replace(",", "/").replace("[", "")
                .replace("]", "");
            var rd = boundary["coordinates"][0][2];
            rd = JSON.stringify(rd).replace(",", "/").replace("[", "")
                .replace("]", "");
            var ru = boundary["coordinates"][0][3];
            ru = JSON.stringify(ru).replace(",", "/").replace("[", "")
                .replace("]", "");
            var detailextend = [boundary["coordinates"][0][1][0], boundary["coordinates"][0][2][1], boundary["coordinates"][0][3][0], boundary["coordinates"][0][0][1]];
            map.getView().fit((detailextend), /** @type {ol.Size} */(map.getSize()));
            var format = new ol.format.GeoJSON();
            var geometry = format.readGeometry(boundary);
            // Create a feature using the geometry
            var feature = new ol.Feature({
                geometry: geometry
            });
            tempSource.addFeature(feature);
            vectortempLayer = new ol.layer.Vector({
                source: tempSource,
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 255, 255, 0)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'red',
                        width: 2
                    })
                })
            });
            map.addLayer(vectortempLayer);
            //filename = filename.replace(".tar", "");
            var htmltable = "<div class='register ' style='width: 280px; overflow: auto; position: relative;float:right;margin-top:5px;margin-right:7px;margin-buttom:0px;margin-left:0'><form id='Image'  method='post'><div><label style='font-size:22px;font-weight:bold;margin-left:60px;margin-top:5px;padding:5px'>影像详情信息</label></div>  <table width='100%' class='table_cx' border='0'><tr>    <th colspan='6'><p class='biao'>影像基础信息</p>    </th></tr><tr>    <td class='right'>卫星</td>    <td >"
                + satelliteid
                + "</td></tr>     <tr>    <td class='right'>传感器</td>    <td >"
                + sensorid
                + "</td></tr>     <tr>    <td class='right'>采集时间</td>    <td  id='Td6'>"
                + acquisitiontime
                + "</td></tr><tr>    <td class='right'>云量</td>    <td  id='Td10'>"
                + cloudpercent
                + "</td></tr><tr>    <td class='right'>分辨率</td>    <td  id='Td10'>"
                + resolution
                + "</td></tr><tr>    <td class='first right'>轨道圈号</td>    <td  id='orbitnumb'>"
                + orbitid
                + "</td></tr><tr>    <td class='first right'>景Path/Row</td>    <td  id='Td4'>"
                + scenepr
                + "</td></tr><tr>    <td class='right'>接收站</td>    <td  id='rcvstation'>"
                + receivestationid
                + "</td></tr><tr>    <td class='first right'>左上角经/纬度</td>    <td  id='leftup'>"
                + lu
                + "</td>   </tr><tr>    <td class='right'>右上角经/纬度</td>    <td  id='Td3'>"
                + ru
                + "</td></tr><t>    <td class='right'>右下角经/纬度</td>    <td  id='Td7'>"
                + rd
                + "</td></t><tr>    <td class='right'>左下角经/纬度</td>    <td  id='Td1'>"
                + ld
                + "</td></tr><tr>    <td class='right'>产品下载</td>    <td  id='download'>"
                + "<a href=download?filename="
                + filename+ ' target="_blank"'
                + ">下载链接</a>"
                + "</td></tr>    </table></form>    </div>";
            //jbox全局设置：
            var jBoxConfig = {};
            jBoxConfig.defaults = {
                top: '80px',
                closed: function () {
                    removetemply();
                }
            }
            $.jBox.setDefaults(jBoxConfig);
            var imghtml = "<img  src='"
                + quickviewuri
                + "' style='width:500px;margin-top:5px;margin-right:0;margin-buttom:0px;margin-left:5px'/>"
            var html = imghtml + htmltable;
            $.jBox(html, {
                title: "影像详情",
                width: 805,
                height: 545,
                buttons: {}
            });
        }
    }
}
//删除临时高亮图层
function removetemply() {
    if (tempSource) {
        tempSource.clear();
    }
    map.removeLayer(vectortempLayer);
}
// 此处可以为某个按钮触发时间对表格的刷新或者其他操作
$("#search_btn").click(function () {
    vectorSource.clear();
});
// 时间控件限定
var start = {
    elem: '#begintime',
    format: 'YYYY/MM/DD',
    min: '2000-06-16', // 设定最小日期为当前日期
    max: laydate.now(), // 最大日期
    istime: true,
    istoday: false,
    choose: function (datas) {
        end.min = datas; // 开始日选好后，重置结束日的最小日期
        end.start = datas // 将结束日的初始值设定为开始日
    }
};
var end = {
    elem: '#endtime',
    format: 'YYYY/MM/DD',
    min: '2099-06-16',
    max: laydate.now(),
    istime: true,
    istoday: true,
    choose: function (datas) {
        start.max = datas; // 结束日选好后，重置开始日的最大日期
    }
};
laydate(start);
laydate(end);
// 时间秒数格式化
function FormatDate(strTime) {
    var date = new Date(strTime);
    return date.getFullYear() + "/" + (date.getMonth() + 1) + "/"
        + date.getDate();
}

// <!--shp添加代码-->
var styleFunction = function (feature, resolution) {
    return styles[feature.getGeometry().getType()];
};
// 导入shapefile矢量文件
function loadShpZip() {
    qtype = "importshp";
    if (vectorSources) {
        vectorSources.clear();
    }
    if (shpluotu) {
        shpluotu = null;
    }
    var epsg = ($('#epsg').val() == '') ? 4326 : $('#epsg').val(), encoding = ($(
        '#encoding').val() == '') ? 'UTF-8' : $('#encoding').val();
    if (file.name.split('.')[1] == 'zip') {
        if (file)
            $('.dimmer').addClass('active');
        loadshp(
            {
                url: file,
                encoding: encoding,
                EPSG: epsg
            },
            function (data) {
                var URL = window.URL || window.webkitURL || window.mozURL
                    || window.msURL, url = URL
                    .createObjectURL(new Blob([JSON.stringify(data)],
                        {
                            type: "application/json"
                        }));
                var importshpjson = {};
                importshpjson['type'] = 'FeatureCollection';
                importshpjson['features'] = [];
                for (var i = 0; i < data["features"].length; i++) {
                    var geojsonObject1 = data["features"][i]["geometry"]["coordinates"];
                    var newFeature = {
                        "type": "Feature",
                        "geometry": {
                            "type": "MultiPolygon",
                            "coordinates": [geojsonObject1]
                        }
                    }
                    importshpjson['features'].push(newFeature);
                }
                document.getElementById("shpinput").value = importshpjson;
                if (vectorSource) {
                    vectorSource.clear();
                }
                vectorSources = new ol.source.Vector({
                    features: (new ol.format.GeoJSON()).readFeatures(importshpjson)
                });
                map.removeLayer(vectorLayer);
                vectorLayer = new ol.layer.Vector({
                    source: vectorSources,
                    style: styleFunction
                });
                map.addLayer(vectorLayer);
                map.getView().fit((data.bbox), /** @type {ol.Size} */
                    (map.getSize()));
            });
    } else {
        $('.modal').modal('show');
    }
}
$("#file").change(function (evt) {
    file = evt.target.files[0];
    if (file.size > 0) {
        $('#dataInfo').text(' ').append(file.name + ' , ' + file.size + ' kb');
        loadShpZip();
    }
});
$('#addZipfile').click(function () {
    show_Dialog();
    $('.shp-modal').toggleClass('effect');
    $('.overlay').toggleClass('effect');
    $('#wrap').toggleClass('blur');
});
$('#encoding').dropdown();
$('.v')
    .change(
    function () {
        var msg = '<div class="msg" id="msg" style="display: none;"><div class="ui primary inverted red segment">'
            + '<p>You can find the EPSG Code of your Shapefile on <strong>spatialreference.org</strong></p></div><br /></div>';
        if ($('#epsg').val().match(/^\d{4}$/) != null) {
            $('#zipfile').removeClass('disabled');
            $('.msg').slideUp(750);
        } else {
            if ($('.msg')[0] == undefined) {
                $('#epsgField').after(msg);
                $('.msg').slideDown(1500);
            }
        }
    });
//加载矢量边框
function AddAllJson_notrans(Allpostjson) {
    var jsonobject = new Array();
    var featureslist = new Array();
    for (var i = 0; i < Allpostjson.length; i++) {
        var coord = Allpostjson[i]["boundary"]["coordinates"];
        jsonobject[i] = {
            'type': 'FeatureCollection',
            'features': [{
                'type': 'Feature',
                'geometry': {
                    'type': 'MultiPolygon',
                    'coordinates': [coord,]
                }
            },]
        };
        featureslist[i] = (new ol.format.GeoJSON()).readFeatures(jsonobject[i]);
        vectorSourceOutline.addFeatures(featureslist[i]);
    }
    /*map.getView().setCenter(
     [ Allpostjson[0]["boundary"]["coordinates"][0][1][0],
     Allpostjson[0]["boundary"]["coordinates"][0][2][1] ]);
     map.getView().setZoom(7);*/
    map.addLayer(vectorLayerOutline);
}
// 画行政界线函数（有判断其为polygon还是mutipolygon）
function DrawXZline(Allpostjson) {
    if (vectorSources) {
        vectorSources.clear();
    }
    var extent;
    for (var i = 0; i < Allpostjson.length; i++) {
        var obj = Allpostjson[i];
        var boundary = obj['boundary'];
        // Read the GeoJSON geometries using the format class
        var format = new ol.format.GeoJSON();
        var geometry = format.readGeometry(boundary);
        // Create a feature using the geometry
        var feature = new ol.Feature({
            geometry: geometry
        });
        // Add feature to the source of some vector layer
        vectorSources.addFeature(feature);
        if (i == 0) {
            extent = geometry.getExtent();
        } else {
            var extent_i = geometry.getExtent();
            if (extent_i[0] < extent[0]) {
                extent[0] = extent_i[0];
            }
            if (extent_i[1] < extent[1]) {
                extent[1] = extent_i[1];
            }
            if (extent_i[2] > extent[2]) {
                extent[2] = extent_i[2];
            }
            if (extent_i[3] > extent[3]) {
                extent[3] = extent_i[3];
            }
        }
    }
    map.getView().fit(extent, /** @type {ol.Size} */
        (map.getSize()));
    map.addLayer(vectorLayer);
}
//结果列表数据全局变量
var mydata = [];
//生成结果列表数据
function Concreatdata(Allpostjson) {
    for (var i = 0; i < Allpostjson.length; i++) {
        var date = new Date(Allpostjson[i]["acquisitiontime"]);
        var mydate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/"
            + date.getDate();
        var cloud = Allpostjson[i]["cloudpercent"] + "%";
        var datas = Allpostjson[i]["thumbview"];
        var temp = "data:image/jpeg;base64," + datas;
        var thumbsrc = "<img  src='" + temp + "'/>";
        mydata.push({
            filename: Allpostjson[i]["filename"],
            thumb: thumbsrc,
            sat: Allpostjson[i]["satelliteid"],
            sensor: Allpostjson[i]["sensorid"],
            date: mydate,
            cloud: cloud
        });
    }
}
// 显示查询结果函数（结果列表+边框加载）
function ShowSearch(allpostjson) {
    Allpostjson = allpostjson;
    //显示结果列表
    Concreatdata(allpostjson);
    // 边框加载
    AddAllJson_notrans(allpostjson);
    // 结果列表控件绑定
    for (var i = 0; i <= mydata.length; i++) {
        jQuery("#list4").jqGrid('addRowData', i + 1, mydata[i]);
    }
    mydata = [];
}
//选择矢量落图框
if (shpluotu) {
    var select = null;
    var selectSingleClick = new ol.interaction.Select();
    select = selectSingleClick;
    if (select !== null) {
        map.addInteraction(select);
        select.on('select', function (e) {
        });
    }
}

