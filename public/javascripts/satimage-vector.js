/*-------------------全局对象及变量声明-----------------*/

//矢量图层样式
var styles = {
    'MultiPolygon': [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'red',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 0, 0)'
        })
    })],
    'Polygon': [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'red',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 0, 0)'
        })
    })]
};
var styleFunction = function (feature, resolution) {
    return styles[feature.getGeometry().getType()];
};
//落图图层样式
var Outlinestyles = {
    'MultiPolygon': [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'blue',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 0, 0)'
        })
    })],
    'Polygon': [new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'blue',
            lineDash: [4],
            width: 3
        }),
        fill: new ol.style.Fill({
            color: 'rgba(0, 0, 255, 0)'
        })
    })]
};
var OutlinestyleFunction = function (feature, resolution) {
    return Outlinestyles[feature.getGeometry().getType()];
};
/*全局的图层和图层源声明*/
var shpluotu;	//全局变量存储落图JSON
//查询到数据的外边框矢量图层源
var vectorSourceOutline = new ol.source.Vector({
    /*features: (new ol.format.GeoJSON()).readFeatures(geojsonObj)*/
});
//查询到数据的外边框矢量图层
var vectorLayerOutline = new ol.layer.Vector({
    source: vectorSourceOutline,
    style: OutlinestyleFunction
});
//贴图图层
var TietuLayerArray = new Array();
var LuotushpArray = new Array();
//将绘制图层、导入图层、经纬度图层以及省市界图层合并为一个图层
///全局的矢量图层源
var vectorSources = new ol.source.Vector({
    /*features: (new ol.format.GeoJSON()).readFeatures(geojsonObj)*/
});
var vectorLayer;  //全局的矢量图层
var querygeojson;  //矢量图层往后台发送的数据
var vectortempLayer;  //选择高亮图层
var tempSource = new ol.source.Vector({
    wrapX: false
});
var queryresultjson; //查询到的结果转JSON数据
var hostport=document.location.host; //服务端ip地址

		