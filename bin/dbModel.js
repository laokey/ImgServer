/**
 * Created by liuhj on 2015/9/6.
 */

var mongoose = require('mongoose');
//sample : mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]
var host = require('./config.json').host;
var port = require('./config.json').port;
var dbname = require('./config.json').dbname;
var connstr = 'mongodb://' + host + ':' + port + '/' + dbname;
mongoose.connect(connstr);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
    console.log('mongodb connection success');
});

/**
 * 推送用户结构
 */
var PushuserSchema;
PushuserSchema = new mongoose.Schema({
    Name: { type: String },
    Password: { type: String },
    Promission: {},
    Priority: Number,
    Boundary: { type: { type: String }, coordinates: [] }
}, {collection : 'pushuser' });
var pushuserModel = db.model('pushuser', PushuserSchema);

/**
 * 元数据结构
 */
var MetaSchema = new mongoose.Schema({
    productid: Number,
    filename: String,
    satelliteid: String,
    receivestationid: String,
    sensorid: String,
    acquisitiontime: Date,
    inputtime: Date,
    cloudpercent: Number,
    sceneid: Number,
    orbitid: Number,
    scenepath: Number,
    scenerow: Number,
    taruri: String,
    quickviewuri: String,
    boundary: { type: { type: String }, coordinates: [] },
    thumbview: {},
    metadata: {}
}, {collection : 'metadata' });
var metaModel = db.model('meta', MetaSchema);

/**
 * 登录用户结构
 */
var UserSchema = new mongoose.Schema({
    name: { type: String },
    password: { type: String }
}, {collection : 'user' });
var userModel=db.model('user', UserSchema);

/**
 * 省市县边界结构
 */
var ExtentSchema = new mongoose.Schema({
    name: { type: String },
    regionid: { type: String },
    boundary: { type: { type: String }, coordinates: [] }
}, {collection : 'chinaBoundary' });
var extentModel = db.model('extent', ExtentSchema);

//exports.PushuserSchema = PushuserSchema;
//exports.pushuserModel = pushuserModel;
exports.MetaSchema = MetaSchema;
exports.metaModel = metaModel;
//exports.UserSchema = UserSchema;
//exports.userModel = userModel;
exports.ExtentSchema = ExtentSchema;
exports.extentModel = extentModel;