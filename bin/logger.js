/**
 * Created by liuhj on 2015/9/17.
 */

var Logger = require('bunyan');

var path = require('./config.json').logfile;
//保存日志文件
var log = Logger.createLogger({
    name: 'distribute',
    streams: [{
        path: path
    }]
});

function info(msg){
    log.info(msg);
}

function warn(msg){
    log.warn(msg);
}

function error(msg){
    log.error(msg);
}

//exports.log = log;
exports.info = info;
exports.warn = warn;
exports.error = error;